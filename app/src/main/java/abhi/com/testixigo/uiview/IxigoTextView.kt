package abhi.com.testixigo.uiview

import abhi.com.testixigo.R
import android.content.Context
import android.graphics.Typeface
import android.os.Handler
import android.os.HandlerThread
import android.util.AttributeSet

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.provider.FontRequest
import androidx.core.provider.FontsContractCompat


/**
 * Created by abhishek on 08/04/19.
 */

class IxigoTextView : AppCompatTextView {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }

    private fun init() {
        try {
            val request = FontRequest(
                "com.google.android.gms.fonts",
                "com.google.android.gms",
                "open-sans",
                R.array.com_google_android_gms_fonts_certs
            )

            val callback = object : FontsContractCompat.FontRequestCallback() {
                override fun onTypefaceRetrieved(typeface: Typeface?) {
                    setTypeface(typeface)
                }

                override fun onTypefaceRequestFailed(reason: Int) {
                    //                    setTypeface(Typeface.BOLD);
                }
            }
            FontsContractCompat
                .requestFont(
                    context, request, callback,
                    handlerThreadHandler
                )
        } catch (e: Exception) {

        }

    }

    companion object {

        val handlerThreadHandler: Handler
            get() {
                var mHandler: Handler? = null
                if (mHandler == null) {
                    val handlerThread = HandlerThread("fonts")
                    handlerThread.start()
                    mHandler = Handler(handlerThread.looper)
                }
                return mHandler
            }
    }

}