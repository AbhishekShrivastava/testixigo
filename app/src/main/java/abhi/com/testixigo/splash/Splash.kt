package abhi.com.ixigotest.splash

import abhi.com.testixigo.R
import abhi.com.testixigo.flight.FlightActivity
import abhi.com.testixigo.network.ConnectionDetector
import abhi.com.testixigo.network.OpenWifi
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View

class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }

        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

    }

    fun init(){
        if(ConnectionDetector.isConnectedToInternet(this)){
            navigateToFlights()
        }else{
            showNetworkError()
        }
    }

    fun navigateToFlights(){
//        Thread.sleep(3000)
        Handler().postDelayed({
            /* Create an Intent that will start the Menu-Activity. */
            val mainIntent = Intent(this, FlightActivity::class.java)
            startActivity(mainIntent)
            finish()
        }, 2000)
    }

    fun showNetworkError(){
        OpenWifi.openWifiSetting(this,111)
    }


    override fun onResume() {
        super.onResume()
        init()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 111 && resultCode == Activity.RESULT_OK){
            init()
        }
    }

}
