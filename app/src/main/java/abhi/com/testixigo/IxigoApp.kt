package abhi.com.testixigo

import abhi.com.testixigo.network.NukeSSLCerts
import abhi.com.testixigo.network.volley.VolleyService
import android.app.Application
import android.content.Context



class IxigoApp : Application(){
    companion object {
        var context: Context? = null


    }

    override fun onCreate() {
        super.onCreate()
        context = this
        NukeSSLCerts.nuke()
        VolleyService.initialize(this)
    }



}