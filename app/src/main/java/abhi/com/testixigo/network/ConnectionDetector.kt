package abhi.com.testixigo.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import java.io.Serializable

class ConnectionDetector : Serializable {

    companion object {
        fun isConnectedToInternet(context: Context) : Boolean{
            val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivity != null) {
                val activeNetwork = connectivity.activeNetworkInfo
                val info = connectivity.allNetworkInfo
                if (info != null)
                    for (i in info.indices)
                        if (info[i].state == NetworkInfo.State.CONNECTED) {
                            return true
                        }

            }
            return false
        }

    }
}