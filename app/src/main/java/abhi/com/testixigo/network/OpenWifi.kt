package abhi.com.testixigo.network

import abhi.com.testixigo.R
import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.provider.Settings
import android.view.LayoutInflater

object OpenWifi {

    fun openWifiSetting(context: Context, reqCode: Int) {
        //        NoInternetDialog builder = new NoInternetDialog.Builder(context).build();

        //
        val li = LayoutInflater.from(context)
        val promptsView = li.inflate(R.layout.dialog_box_net_connection, null)
        val alertDialogBuilder = android.app.AlertDialog.Builder(context)
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton("Open Settings"
            ) { dialog, id ->
                (context as Activity).startActivityForResult(Intent(Settings.ACTION_WIFI_SETTINGS), reqCode)
                try {
                    context.startActivityForResult(Intent(Settings.ACTION_WIFI_SETTINGS), reqCode)
                } catch (e: ActivityNotFoundException) {

                }
            }
            .setNegativeButton("Skip"
            ) { dialog, id ->
                dialog.cancel()
                (context as Activity).finish()
            }

        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()

    }


}
