package abhi.com.ixigotest.network


interface NTCallback {
    fun onSuccess(result: String)
    fun onError(error: ApiError)
}