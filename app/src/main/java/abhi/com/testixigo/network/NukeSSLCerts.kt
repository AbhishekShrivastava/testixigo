package abhi.com.testixigo.network

import java.security.SecureRandom
import java.security.cert.CertificateException

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.cert.X509Certificate

object NukeSSLCerts {
    internal val TAG = "NukeSSLCerts"

    fun nuke() {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    x509Certificates: Array<java.security.cert.X509Certificate>,
                    s: String
                ) {

                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    x509Certificates: Array<java.security.cert.X509Certificate>,
                    s: String
                ) {

                }

                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }
            })

            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
            HttpsURLConnection.setDefaultHostnameVerifier { arg0, arg1 -> true }
        } catch (e: Exception) {
        }

    }
}