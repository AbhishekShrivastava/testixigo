package abhi.com.ixigotest.network

import java.lang.Exception

class ApiError : Exception {
    var responseCode: Int = 0
    override var message: String

    constructor(responseCode: Int) {
        this.responseCode = responseCode
        this.message = ""
    }

    constructor(responseCode: Int, message: String) {
        this.responseCode = responseCode
        this.message = message
    }
}