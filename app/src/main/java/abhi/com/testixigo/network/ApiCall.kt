package abhi.com.ixigotest.network

import abhi.com.testixigo.network.volley.VolleyService
import android.content.Context
import android.util.Log
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import org.json.JSONException
import java.io.UnsupportedEncodingException

class ApiCall(internal var context : Context){
    init {

    }

    fun callGetApi(URL : String, NTCallback: NTCallback){
        val request = StringRequest(Request.Method.GET, URL,
            Response.Listener { response ->
               NTCallback.onSuccess(response)
            },
            Response.ErrorListener { error ->
                try {
                    val response = error.networkResponse
                    var message = ""
                    var statusCode = 503
                    if (response != null) {
                        statusCode = error.networkResponse.statusCode

                    }
                    NTCallback.onError(ApiError(statusCode, message))
                } catch (e1: UnsupportedEncodingException) {
                    // Couldn't properly decode data to string
                    e1.printStackTrace()
                } catch (e2: JSONException) {
                    // returned data is not JSONObject?
                    e2.printStackTrace()
                }
            })

        request.retryPolicy = DefaultRetryPolicy(30000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        VolleyService.requestQueue.add(request)
        VolleyService.requestQueue.start()
    }

}