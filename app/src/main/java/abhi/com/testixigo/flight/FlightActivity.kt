package abhi.com.testixigo.flight

import abhi.com.ixigotest.flight.FlightAdapter
import abhi.com.ixigotest.flight.FlightListModel
import abhi.com.ixigotest.network.ApiCall
import abhi.com.ixigotest.network.ApiError
import abhi.com.ixigotest.network.NTCallback
import abhi.com.testixigo.R
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import org.json.JSONObject

class FlightActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var progressBar: ProgressBar
    private var recyclerView: RecyclerView? = null
    var flightList : ArrayList<FlightListModel> = ArrayList()
    var adapterFlights = FlightAdapter(flightList)

    var isLastPage: Boolean = false //TODO for pagination
    var isLoading: Boolean = false //TODO for pagination

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initVars()
    }

    @SuppressLint("WrongConstant")
    fun initVars(){
        recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)

        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView?.layoutManager = layoutManager
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.adapter = adapterFlights

        val speedDialView = findViewById<SpeedDialView>(R.id.speedDial)
        speedDialView.addActionItem(SpeedDialActionItem.Builder(R.id.fab_price, R.drawable.ic_currency).create())
        speedDialView.addActionItem(SpeedDialActionItem.Builder(R.id.fab_arrival, R.drawable.ic_arrivals).create())
        speedDialView.addActionItem(SpeedDialActionItem.Builder(R.id.fab_departure, R.drawable.ic_departures).create())

        speedDialView.setOnActionSelectedListener(SpeedDialView.OnActionSelectedListener { actionItem ->
            when (actionItem.id) {
                R.id.fab_price -> {
                    flightList.sortWith(compareBy<FlightListModel>{it.fare})
                    adapterFlights.notifyDataSetChanged()
                    speedDialView.close()
                    return@OnActionSelectedListener true // false will close it without animation
                }
                R.id.fab_arrival -> {
                    flightList.sortWith(compareBy<FlightListModel>{it.arrivalTime})
                    adapterFlights.notifyDataSetChanged()
                    speedDialView.close() // To close the Speed Dial with animation
                    return@OnActionSelectedListener true // false will close it without animation
                }
                R.id.fab_departure -> {
                    flightList.sortWith(compareBy<FlightListModel>{it.departureTime})
                    adapterFlights.notifyDataSetChanged()
                    speedDialView.close() // To close the Speed Dial with animation
                    return@OnActionSelectedListener true // false will close it without animation
                }
            }
            false
        })

        callFlights()

    }

    fun callFlights(){
        progressBar.visibility = View.VISIBLE
        var apiCall = ApiCall(this)
        apiCall.callGetApi("https://www.mocky.io/v2/5979c6731100001e039edcb3",object : NTCallback {
            @SuppressLint("WrongConstant")
            override fun onSuccess(result:String) {
//                Log.e("Api result ",result)
                progressBar.visibility = View.GONE

                var jsonObject = JSONObject(result)
                if(jsonObject.optJSONArray("flights") != null){
                    for(i in 0 until jsonObject.optJSONArray("flights").length()) {
                        for (j in 0 until jsonObject.optJSONArray("flights").optJSONObject(i).optJSONArray(
                            "fares"
                        ).length()) {
                            var flightObj = FlightListModel(
                                jsonObject.optJSONArray("flights").optJSONObject(i),
                                jsonObject.optJSONObject("appendix"),
                                jsonObject.optJSONArray("flights").optJSONObject(i).optJSONArray("fares").optJSONObject(
                                    j
                                ).optInt("fare"),
                                jsonObject.optJSONArray("flights").optJSONObject(i).optJSONArray("fares").optJSONObject(
                                    j
                                ).optInt("providerId")
                            )
                            flightList.add(flightObj)
                        }
                    }

                }else{
                    /* TODO: for pagination
                    isLatPage = true
                     */
                }

                adapterFlights.notifyDataSetChanged()

            }
            override fun onError(error: ApiError){
                Log.e("Api error ",error.toString())
                progressBar.visibility = View.GONE
            }
        })

    }

    /* TODO: for pagination
    fun getMoreItems() {
    //after fetching your data assuming you have fetched list in your
    // recyclerview adapter assuming your recyclerview adapter is
    //rvAdapter
    after getting your data you have to assign false to isLoading
    isLoading = false

    rvAdapter.addData(flightList)
}
     */

    override fun onResume() {
        super.onResume()

    }

    override fun onClick(p0: View?) {

    }

}

