package abhi.com.ixigotest.flight

import abhi.com.testixigo.R
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import java.sql.Timestamp
import java.util.*


/**
 * Created by bett on 8/26/17.
 */

class FlightAdapter(private var items: ArrayList<FlightListModel>): RecyclerView.Adapter<FlightAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.item_flight_list, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FlightAdapter.ViewHolder, position: Int) {
        var flightObj = items[position]
        holder.airlineImage?.setImageResource(flightObj.flightImage)
        holder?.time?.text = flightObj.flightTime
        holder?.duration?.text = flightObj.duration
        holder?.classType?.text = flightObj.classType
        holder?.airport?.text = flightObj.airportDep
        holder?.fare?.text = "INR " + flightObj.fare.toString()
        holder?.provider?.text = flightObj.provider

    }

    override fun getItemCount(): Int {
        return items.size
    }

/* TODO for pagination

fun addData(listItems: ArrayList<FlightListModel>) {
    var size = this.FlightListModel.size
    this.FlightListModel.addAll(FlightListModel)
    var sizeNew = this.FlightListModel.size
    notifyItemRangeChanged(size, sizeNew)
}
 */


    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var airlineImage: AppCompatImageView? = null
        var time: TextView? = null
        var duration: TextView? = null
        var classType: TextView? = null
        var airport: TextView? = null
        var fare: TextView? = null
        var provider: TextView? = null

        var airlineCode: TextView? = null


        init {
            this.airlineImage = row?.findViewById(R.id.im_airline)
            this.time = row?.findViewById<TextView>(R.id.tv_timing)
            this.duration = row?.findViewById<TextView>(R.id.tv_duration)
            this.classType = row?.findViewById<TextView>(R.id.tv_class)
            this.airport = row?.findViewById<TextView>(R.id.tv_airport)
            this.fare = row?.findViewById<TextView>(R.id.tv_fare)
            this.provider = row?.findViewById<TextView>(R.id.tv_airline_provide)

            this.classType = row?.findViewById<TextView>(R.id.tv_class)
            this.classType = row?.findViewById<TextView>(R.id.tv_class)


        }
    }
}