package abhi.com.ixigotest.flight

import abhi.com.testixigo.R
import android.graphics.drawable.Drawable
import android.util.Log
import org.json.JSONObject
import java.sql.Time
import java.sql.Timestamp
import java.util.*
import java.util.logging.LogManager


class FlightListModel() {
    /*
   provider-wise fares,  airport names, , etc.

     */
    var flightImage : Int = 0
    var flightTime : String = ""
    var classType : String = ""
    var airlineCode : String = ""
    var duration : String = ""
    var fare : Int = 0
    var provider : String = ""
    var airportDep : String = ""
    var airportArr : String = ""
    var arrivalTime : Long = 0
    var departureTime : Long = 0

    constructor(jsonObject: JSONObject,appendixObj : JSONObject,fare : Int,provider:Int) : this() {
            //Iterating through each provider for getting all flights in simple obj
            for (i in 0..jsonObject.optJSONArray("fares").length()-1) {
                this.flightImage = getFlightImage(jsonObject.optString("airlineCode"))
                this.flightTime = getTiming(jsonObject.optLong("arrivalTime"),jsonObject.optLong("departureTime"))
                this.duration = getFlightDuration(jsonObject.optLong("arrivalTime"),jsonObject.optLong("departureTime"))
                this.arrivalTime = jsonObject.optLong("arrivalTime")
                this.departureTime = jsonObject.optLong("departureTime")
                this.airportDep = appendixObj.optJSONObject("airports").optString(jsonObject.optString("originCode"))
                this.airportArr = jsonObject.optString("destinationCode")
                this.fare = fare
                this.provider = getFlightProvider(appendixObj.optJSONObject("providers"),provider)
                this.airlineCode = jsonObject.optString("airlineCode")
                this.classType = jsonObject.optString("class")

            }

//        }
    }

    private fun getTiming(arr : Long,dept : Long) : String{
        val stamp = Timestamp(arr)
        val date = Date(stamp.getTime())

        val stamp1 = Timestamp(dept)
        val date1 = Date(stamp1.getTime())

        Log.e("hbb",date1.toString())

        return date1.hours.toString() + ":" + date1.minutes.toString() + " - " + date.hours.toString() + ":" + date.minutes.toString()
    }

    fun getFlightDuration(arr : Long,dept : Long) : String{
        val stamp = Timestamp(arr)
        val date = Date(stamp.getTime())

        val stamp1 = Timestamp(dept)
        val date1 = Date(stamp1.getTime())

        var timeDiff = date.hours - date1.hours

        var timeDiffMin = date.minutes - date1.minutes

        return timeDiff.toString() + "h " + timeDiffMin + "min"


    }

    fun getFlightProvider(idProvider : JSONObject,id : Int) : String{
      return idProvider.optString(id.toString())
    }

    fun getFlightImage(providerId : String) : Int{
        when (providerId) {
            "SG" -> return R.drawable.spicejet
            "AI" -> return R.drawable.airindia
            "G8" -> return R.drawable.ic_goair
            "9W" -> return R.drawable.jetairways
            "6E" -> return R.drawable.indigo
            else -> { // Note the block
                return R.drawable.indigo
            }
        }
    }

}